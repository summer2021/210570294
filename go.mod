module github.com/beyondstorage/go-service-ipfs

go 1.15

require (
	github.com/beyondstorage/go-endpoint v1.0.1
	github.com/beyondstorage/go-integration-test/v4 v4.1.2-0.20210702060531-e08a973380df
	github.com/beyondstorage/go-storage/v4 v4.2.1-0.20210705112743-4787bc39d185
	github.com/google/uuid v1.2.0
	github.com/ipfs/go-ipfs-api v0.2.0
	github.com/ipfs/go-ipfs-cmds v0.6.0
)
